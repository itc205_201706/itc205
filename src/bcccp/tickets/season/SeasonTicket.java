package bcccp.tickets.season;

import java.util.ArrayList;
import java.util.List;

public class SeasonTicket implements ISeasonTicket {
	
	private List<IUsageRecord> usages;
	private IUsageRecord currentUsage = null;
	
	private String ticketId;
	private String carparkId;
	private long startValidPeriod;
	private long endValidPeriod;
	
	public SeasonTicket (String ticketId, 
			             String carparkId, 
			             long startValidPeriod,
			             long endValidPeriod) {
		this.ticketId = ticketId;
		this.carparkId = carparkId;
		this.startValidPeriod = startValidPeriod;
		this.endValidPeriod = endValidPeriod;
		this.usages = new ArrayList<IUsageRecord>();
				
	}

	@Override
	public String getId() {
		return this.ticketId;
	}

	@Override
	public String getCarparkId() {
		return this.carparkId;
	}

	@Override
	public long getStartValidPeriod() {
		return this.startValidPeriod;
	}

	@Override
	public long getEndValidPeriod() {
		return this.endValidPeriod;
	}

	@Override
	public boolean inUse() {
		if (this.currentUsage != null) {
			return true;
		}
		else return false;
			
	}

	@Override
	public void recordUsage(IUsageRecord record) {
		this.currentUsage = record;
	}

	@Override
	public IUsageRecord getCurrentUsageRecord() {
		return this.currentUsage;
	}

	@Override
	public void endUsage(long dateTime) {
		this.currentUsage.finalise(dateTime);
		this.usages.add(this.currentUsage);
		this.currentUsage = null;
	}

	@Override
	public List<IUsageRecord> getUsageRecords() {
		return this.usages;
	}


}
