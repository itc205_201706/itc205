package bcccp.tickets.season;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.IUsageRecordFactory;

public class SeasonTicketDAO implements ISeasonTicketDAO {

	private IUsageRecordFactory factory;
	private List<ISeasonTicket> tickets;

	
	
	public SeasonTicketDAO(IUsageRecordFactory factory) {
		this.factory = factory;
		this.tickets = new ArrayList<ISeasonTicket>();
	}



	@Override
	public void registerTicket(ISeasonTicket ticket) {
		this.tickets.add(ticket);		
	}



	@Override
	public void deregisterTicket(ISeasonTicket ticket) {
		this.tickets.remove(ticket);		
	}



	@Override
	public int getNumberOfTickets() {
		return this.tickets.size();
	}



	@Override
	public ISeasonTicket findTicketById(String ticketId) {
		for (ISeasonTicket ticket : this.tickets) {
			if (ticket.getId() == ticketId) {
				return ticket;
			}				
		}
		return null;
	}



	@Override
	public void recordTicketEntry(String ticketId) {
		Date date = new Date();
		for (ISeasonTicket ticket : this.tickets) {
			if (ticket.getId() == ticketId) {
				IUsageRecord record = factory.make(ticketId, date.getTime());
				ticket.recordUsage(record);
			}				
		}
	}



	@Override
	public void recordTicketExit(String ticketId) {
		Date date = new Date();
		for (ISeasonTicket ticket : this.tickets) {
			if (ticket.getId() == ticketId) {
				ticket.endUsage(date.getTime());
			}				
		}
	}
	
	
	
}
