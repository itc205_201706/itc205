package bcccp.tickets.adhoc;

import java.util.List;
import java.util.ArrayList;

import bcccp.carpark.ICarSensorResponder;

public class AdhocTicketDAO  implements IAdhocTicketDAO  {

	private IAdhocTicketFactory factory;
	private int currentTicketNo;
	private List<IAdhocTicket> tickets;

	
	
	public AdhocTicketDAO(IAdhocTicketFactory factory) {
		this.factory = factory;
		this.currentTicketNo = 0;
		this.tickets = new ArrayList<IAdhocTicket>();
	}



	@Override
	public IAdhocTicket createTicket(String carparkId) {
		this.currentTicketNo++;
		IAdhocTicket newTicket = factory.make(carparkId, this.currentTicketNo);
		tickets.add(newTicket);
		return newTicket;
	}



	@Override
	public IAdhocTicket findTicketByBarcode(String barcode) {
		for (IAdhocTicket ticket : this.tickets) {
			if (ticket.getBarcode() == barcode) {
				return ticket;
			}				
		}
		return null;
	}



	@Override
	public List<IAdhocTicket> getCurrentTickets() {
		List<IAdhocTicket> currentTickets = new ArrayList<IAdhocTicket>();
		for (IAdhocTicket ticket : this.tickets) {
			if (ticket.isCurrent()) {
				currentTickets.add(ticket);
			}				
		}
		return currentTickets;
	}

	
	
}
