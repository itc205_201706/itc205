package bcccp.tickets.adhoc;

import java.util.Date;

public class AdhocTicket implements IAdhocTicket {
	
	private String carparkId;
	private int ticketNo;
	private long entryDateTime;
	private long paidDateTime;
	private long exitDateTime;
	private float charge;
	private String barcode;

	
	
	public AdhocTicket(String carparkId, int ticketNo, String barcode) {
		this.carparkId = carparkId;
		this.ticketNo = ticketNo;
		this.barcode = barcode;
		this.entryDateTime = 0;
		this.paidDateTime = 0;
		this.exitDateTime = 0;
		this.charge = 0;
	}


	@Override
	public int getTicketNo() {
		return this.ticketNo;
	}


	@Override
	public String getBarcode() {
		return this.barcode;
	}


	@Override
	public String getCarparkId() {
		return this.carparkId;
	}


	@Override
	public void enter(long dateTime) {
		this.entryDateTime = dateTime;		
	}


	@Override
	public long getEntryDateTime() {
		return this.entryDateTime;
	}


	@Override
	public boolean isCurrent() {
		if (this.exitDateTime == 0) {
			return true;
		}
		else return false;
	}


	@Override
	public void pay(long dateTime, float charge) {
		this.paidDateTime = dateTime;
		this.charge = charge;		
	}


	@Override
	public long getPaidDateTime() {
		return this.paidDateTime;
	}


	@Override
	public boolean isPaid() {
		if (this.paidDateTime > 0) {
			return true;
		}
		else return false;
	}


	@Override
	public float getCharge() {
		return this.charge;
	}


	@Override
	public void exit(long dateTime) {
		this.exitDateTime = dateTime;		
	}


	@Override
	public long getExitDateTime() {
		return this.exitDateTime;
	}


	@Override
	public boolean hasExited() {
		if (this.exitDateTime > 0) {
			return true;
		}
		else return false;
	}

	
	
}
