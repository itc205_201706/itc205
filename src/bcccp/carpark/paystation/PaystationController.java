package bcccp.carpark.paystation;

import bcccp.carpark.ICarpark;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;

public class PaystationController 
		implements IPaystationController {
	
	private IPaystationUI ui;	
	private ICarpark carpark;

	private IAdhocTicket  adhocTicket = null;
	private float charge;
	
	
	

	public PaystationController(ICarpark carpark, IPaystationUI ui) {
		this.carpark= carpark;
		this.ui = ui;
		ui.registerController(this);
	}



	@Override
	public void ticketInserted(String barcode) {
		// TODO Auto-generated method stub
		try{
			adhocTicket = carpark.getAdhocTicket(barcode);
			charge = carpark.calculateAddHocTicketCharge(adhocTicket.getEntryDateTime());
			ui.display(Float.toString(charge));
		}catch(NullPointerException npe){
			ui.display("Ticket Not Found");
			throw npe;
		}catch(Exception e){
			ui.display("Super Error, call someone");
			throw e;
		}
		
	}



	@Override
	public void ticketPaid() {
		// TODO Auto-generated method stub
		String carparkId;
		int ticketNo;
		long entryTime;
		long paidTime;
		String barcode;
		
		try{
			adhocTicket.pay(System.currentTimeMillis(), charge);
			carparkId = adhocTicket.getCarparkId();
			ticketNo = adhocTicket.getTicketNo();
			entryTime = adhocTicket.getEntryDateTime();
			paidTime = adhocTicket.getPaidDateTime();
			barcode = adhocTicket.getBarcode();
			
			ui.printTicket(carparkId, ticketNo, entryTime, paidTime, paidTime, barcode);
			ui.display("Take Ticket");
		}catch(NullPointerException npe){
			ui.display("Ticket was lost");
		}
		
	}



	@Override
	public void ticketTaken() {
		// TODO Auto-generated method stub
		
	}

	
	
}
