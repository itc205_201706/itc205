package bcccp.carpark.exit;

import bcccp.carpark.Carpark;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarSensorResponder;
import bcccp.carpark.ICarpark;
import bcccp.carpark.IGate;
import bcccp.tickets.adhoc.IAdhocTicket;

public class ExitController 
		implements ICarSensorResponder,
		           IExitController {
	
	private IGate exitGate;
	private ICarSensor insideSensor;
	private ICarSensor outsideSensor; 
	private IExitUI ui;
	
	private ICarpark carpark;
	private IAdhocTicket  adhocTicket = null;
	private long exitTime;
	private String seasonTicketId = null;
	
	

	public ExitController(Carpark carpark, IGate exitGate, 
			ICarSensor is,
			ICarSensor os, 
			IExitUI ui) {
		this.carpark = carpark;
		this.exitGate = exitGate;
		this.insideSensor = is;
		this.outsideSensor = os;
		this.ui = ui;
		
		ui.registerController(this);
		is.registerResponder(this);
		os.registerResponder(this);
	}



	@Override
	public void ticketInserted(String ticketStr) {
		boolean paid;
		try{
			adhocTicket = carpark.getAdhocTicket(ticketStr);
			paid = adhocTicket.isPaid();
			if(paid){
				ui.display("Take Ticket");
			}else{
				ui.display("Please See Attendant");
			}
		}catch(NullPointerException npe){
			ui.display("Ticket not Found: See Attendant");
		}
		
	}



	@Override
	public void ticketTaken() {
		exitGate.raise();
	}



	@Override
	public void carEventDetected(String detectorId, boolean detected) {
		if(detectorId.equals("Exit Inside Sensor") && detected){
			ui.display("Insert Ticket");
		}
		
		if(detectorId.equals("Exit Inside Sensor") && !detected){
			ui.display("");
		}
		
		if(detectorId.equals("Exit Outside Sensor") && !detected){
			exitGate.lower();
			try{
				adhocTicket.exit(System.currentTimeMillis());
				carpark.recordAdhocTicketExit();
			}catch(NullPointerException npe){
				ui.display("No Ticket");
			}
			
		}
		
	}

	
	
}
