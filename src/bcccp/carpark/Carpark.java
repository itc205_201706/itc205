package bcccp.carpark;

import java.util.List;
import java.util.ArrayList;

import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.ISeasonTicketDAO;

public class Carpark implements ICarpark {
	
	private List<ICarparkObserver> observers;
	private String carparkId;
	private int capacity;
	private int numberOfCarsParked;
	private IAdhocTicketDAO adhocTicketDAO;
	private ISeasonTicketDAO seasonTicketDAO;
	
	
	
	public Carpark(String name, int capacity, 
			IAdhocTicketDAO adhocTicketDAO, 
			ISeasonTicketDAO seasonTicketDAO) {
		this.carparkId = name;
		this.capacity = capacity;
		this.numberOfCarsParked = 0;
		this.adhocTicketDAO = adhocTicketDAO;
		this.seasonTicketDAO = seasonTicketDAO;
		this.observers = new ArrayList();
	}



	@Override
	public void register(ICarparkObserver observer) {
		if (this.observers.contains(observer) != true)
		{
			this.observers.add(observer);
		}
	}



	@Override
	public void deregister(ICarparkObserver observer) {
		if (this.observers.contains(observer) == true) {
			this.observers.remove(observer);
		}		
	}



	@Override
	public String getName() {
		return this.carparkId;
	}



	@Override
	public boolean isFull() {
		if (this.numberOfCarsParked < this.capacity) {
			return false;
		}
		else return true;
	}



	@Override
	public IAdhocTicket issueAdhocTicket() {
		return this.adhocTicketDAO.createTicket(this.carparkId)
	}



	@Override
	public void recordAdhocTicketEntry() {
		this.numberOfCarsParked++;		
	}



	@Override
	public IAdhocTicket getAdhocTicket(String barcode) {
		return this.adhocTicketDAO.findTicketByBarcode(barcode);
	}



	@Override
	public float calculateAddHocTicketCharge(long entryDateTime) {
		// Assumptions
		// two rates, business and nonbusiness. business is 6AM to 6PM weekdays.
		// business rate will be 10, nonbusiness 5.
		float charge = 0;
		long diff = System.currentTimeMillis() - entryDateTime;
		long diffHours = diff / (60 * 60 * 1000);
		charge = (float) (diffHours * 3.00); //Difference of hours times hour value
		return charge;
	}



	@Override
	public void recordAdhocTicketExit() {
		this.numberOfCarsParked--;		
	}



	@Override
	public void registerSeasonTicket(ISeasonTicket seasonTicket) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void deregisterSeasonTicket(ISeasonTicket seasonTicket) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public boolean isSeasonTicketValid(String ticketId) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public boolean isSeasonTicketInUse(String ticketId) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public void recordSeasonTicketEntry(String ticketId) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void recordSeasonTicketExit(String ticketId) {
		// TODO Auto-generated method stub
		
	}

	
	

}
